Backup to S3
============
Set the values required in the vars directory for the environment you would like to run
production: rds
staging: localhost

Run this:

```
ansible-playbook backup.yml --extra-vars "env=production"
```
